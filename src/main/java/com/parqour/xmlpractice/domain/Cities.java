package com.parqour.xmlpractice.domain;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@JacksonXmlRootElement
public class Cities implements Serializable {

  private static final long serialVersionUID = 22L;

  @JacksonXmlProperty(localName = "City")
  @JacksonXmlElementWrapper(useWrapping = false)
  private List<City> cities = new ArrayList<>();

}
