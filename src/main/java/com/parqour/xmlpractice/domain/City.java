package com.parqour.xmlpractice.domain;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JacksonXmlRootElement(localName = "City")
public class City implements Serializable {

  private static final long serialVersionUID = 21L;

  @JacksonXmlProperty(isAttribute = true)
  private Long id;

  @JacksonXmlProperty
  private String name;

  @JacksonXmlProperty
  private int population;


}
