package com.parqour.xmlpractice.controller;

import com.parqour.xmlpractice.domain.Cities;
import com.parqour.xmlpractice.domain.City;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/xml")
public class RestXmlController {

  @GetMapping(value = "/cities", produces = MediaType.APPLICATION_XML_VALUE)
  public Cities getCities() {
    Cities cities = new Cities();
    List<City> cityList = List.of(City.builder().id(1L).name("First").population(12323).build());
    cities.setCities(cityList);
    log.debug("Sending back cities: {}", cities);
    return cities;
  }

  @PostMapping(value = "/cities", consumes = MediaType.APPLICATION_XML_VALUE)
  public String addCities(@RequestBody Cities cities) {
    StringBuilder result = new StringBuilder();
    log.debug("Received {}", cities);
    for (City city: cities.getCities()) {
      result.append(" ").append(city.getName());
    }
    return result.toString();
  }
}
